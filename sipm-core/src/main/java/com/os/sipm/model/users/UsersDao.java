/**
 * 
 */
package com.os.sipm.model.users;

import java.util.List;
import java.util.Map;

/**
 * @author kris Dec 18, 2011 7:20:05 PM
 */
public interface UsersDao {

    int saveUsers(Users users);

    int updateUsers(Users users);

    int deleteUsers(Users users);

    Users getUserById(int id);

    Users getUsersByUsernameAndPassword(Map<Object, Object> params);

    List<Users> getUserByUsername(Map<Object, Object> params);

    List<Users> getAllUsers(Map<Object, Object> params);

    int countAllUsers();
}
