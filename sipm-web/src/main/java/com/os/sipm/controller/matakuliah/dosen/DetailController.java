/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.controller.matakuliah.dosen;

import com.os.sipm.controller.OSController;
import com.os.sipm.model.matakuliah.dosen.MataKuliahDosen;
import com.os.sipm.model.matakuliah.dosen.MataKuliahDosenService;
import com.os.sipm.model.matakuliah.MataKuliahService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Label;

/**
 * 
 * @author KrisSadewo
 */
public class DetailController extends OSController {
  
	private static final long serialVersionUID = 2433783679215864116L;
	
	private Label lblNip;
    private Label lblNama;
    private Label lblUts;
    private Label lblUas;
    private Label lblKuis1;
    private Label lblKuis2;
    private Label lblAbsensi;
    private Label lblResponsi;
    private Label lblTahun;
    private Label lblSemester;
    private Label lblJadwal;
    private Label lblKelas;
    private Label lblMataKuliah;   
    private MataKuliahDosen selectedMataKuliahDosen;
    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    protected void initComponent() {
        // Get param from parent window if exist       
        selectedMataKuliahDosen = (MataKuliahDosen) Executions.getCurrent().getArg().get("selectedMataKuliahDosen");
        if (selectedMataKuliahDosen != null) {
            lblNip.setValue(selectedMataKuliahDosen.getDosen().getNip());
            lblNama.setValue(selectedMataKuliahDosen.getDosen().getNama());
            lblUts.setValue(String.valueOf(selectedMataKuliahDosen.getUts()));
            lblUas.setValue(String.valueOf(selectedMataKuliahDosen.getUas()));
            lblKuis1.setValue(String.valueOf(selectedMataKuliahDosen.getKuis1()));
            lblKuis2.setValue(String.valueOf(selectedMataKuliahDosen.getKuis2()));
            lblAbsensi.setValue(String.valueOf(selectedMataKuliahDosen.getAbsensi()));
            lblResponsi.setValue(String.valueOf(selectedMataKuliahDosen.getResponsi()));
            lblSemester.setValue(String.valueOf(selectedMataKuliahDosen.getSemester()));
            lblTahun.setValue(String.valueOf(selectedMataKuliahDosen.getTahun()));
            lblJadwal.setValue(String.valueOf(selectedMataKuliahDosen.getJadwal()));
            lblKelas.setValue(String.valueOf(selectedMataKuliahDosen.getKelas()));
            lblMataKuliah.setValue(selectedMataKuliahDosen.getMataKuliah().getNama());
        }
    }
}
