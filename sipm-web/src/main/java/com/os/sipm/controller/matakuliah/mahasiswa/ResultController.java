/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.controller.matakuliah.mahasiswa;

import com.os.sipm.controller.OSController;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;

/**
 *
 * @author kris
 */
public class ResultController extends OSController {

    Window winResult;

    @Override
    protected void initComponent() {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void onClick$btnCariNip() throws InterruptedException {
        Window window = (Window) Executions.createComponents("/views/matakuliahmahasiswa/list_result.zul", this.self, null);
        window.doModal();
    }

    public void onClose$winResult() {
        this.self.detach();
    }
}
