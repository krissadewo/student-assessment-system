/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.model.matakuliah.mahasiswa;

import java.util.List;
import java.util.Map;

/**
 *
 * @author KrisSadewo
 * Jul 4, 2011   
 * 5:12:15 PM 
 */
public interface MataKuliahMahasiswaDao {

    int saveMataKuliahMahasiswa(Map<Object, Object> mataKuliahMahasiswas);

    int deleteMataKuliahMahasiswa(MataKuliahMahasiswa mataKuliahMahasiswa);

    MataKuliahMahasiswa getMataKuliahMahasiswaById(int id);

    List<MataKuliahMahasiswa> getMataKuliahMahasiswaByNim(Map<Object, Object> params);

    List<MataKuliahMahasiswa> getMataKuliahMahasiswaByName(Map<Object, Object> params);

    List<MataKuliahMahasiswa> getAllMataKuliahMahasiswa(Map<Object, Object> params);

    int countAllMataKuliahMahasiswa();

    int countAllMataKuliahMahasiswaByNim(Map<Object, Object> params);

    int countAllMataKuliahMahasiswaByName(Map<Object, Object> params);
}
