/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.controller.matakuliah.mahasiswa;

import com.os.sipm.controller.OSController;
import com.os.sipm.model.matakuliah.MataKuliah;
import com.os.sipm.model.matakuliah.MataKuliahService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

/**
 *
 * @author kris
 * @created on Jul 6, 2011  4:53:58 PM
 */
public class ListController extends OSController {

    private Combobox cmbboxMataKuliah;
    private Listbox listboxData;
    @Autowired
    private MataKuliahService mataKuliahService;
    private List<MataKuliah> mataKuliahs;

    @Override
    public void initComponent() {
        loadDataMataKuliah();
    }

    private void loadDataMataKuliah() {
        cmbboxMataKuliah.getItems().clear();
        mataKuliahs = mataKuliahService.getAll(null);
        for (MataKuliah mataKuliah : mataKuliahs) {
            Comboitem comboitem = new Comboitem();
            comboitem.setValue(Integer.valueOf(mataKuliah.getId()));
            comboitem.setLabel(mataKuliah.getNama());
            cmbboxMataKuliah.appendChild(comboitem);
        }
        cmbboxMataKuliah.setSelectedIndex(0);
    }

    public void onClick$btnAdd(Event event) throws InterruptedException {
        Window window = (Window) Executions.createComponents("/views/matakuliahmahasiswa/add.zul", this.self, null);
        window.doModal();
//        List<MataKuliahDosen> mataKuliahDosenTemp = (List<MataKuliahDosen>) window.getAttribute("mataKuliahDosens");
//        if (mataKuliahDosenTemp != null) {
//            mataKuliahDosens = mataKuliahDosenTemp;
//            this.generateLisboxData(1);
//        }
    }
}
