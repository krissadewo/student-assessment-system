/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.model.matakuliah.mahasiswa;

import com.os.sipm.model.mahasiswa.Mahasiswa;
import com.os.sipm.model.matakuliah.MataKuliah;

/**
 *
 * @author KrisSadewo
 * Jul 4, 2011   
 * 4:51:20 PM 
 */
public class MataKuliahMahasiswa {

    private Integer id;
    private MataKuliah mataKuliah;
    private Mahasiswa mahasiswa;
    private boolean batal;

    public boolean isBatal() {
        return batal;
    }

    public void setBatal(boolean batal) {
        this.batal = batal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(MataKuliah mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MataKuliahMahasiswa other = (MataKuliahMahasiswa) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.mataKuliah != other.mataKuliah && (this.mataKuliah == null || !this.mataKuliah.equals(other.mataKuliah))) {
            return false;
        }
        if (this.mahasiswa != other.mahasiswa && (this.mahasiswa == null || !this.mahasiswa.equals(other.mahasiswa))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 59 * hash + (this.mataKuliah != null ? this.mataKuliah.hashCode() : 0);
        hash = 59 * hash + (this.mahasiswa != null ? this.mahasiswa.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "MataKuliahMahasiswa{" + "id=" + id + ", mataKuliah=" + mataKuliah + ", mahasiswa=" + mahasiswa + '}';
    }
}
