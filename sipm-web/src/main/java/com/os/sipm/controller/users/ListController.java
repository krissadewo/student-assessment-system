/**
 * 
 */
package com.os.sipm.controller.users;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Toolbarbutton;
import org.zkoss.zul.event.PagingEvent;

import com.os.sipm.controller.OSController;
import com.os.sipm.model.mahasiswa.Mahasiswa;
import com.os.sipm.model.users.Users;

/**
 * @author kris
 * 
 */
public class ListController extends OSController {

	private static final long serialVersionUID = -3252821888522667199L;
	private Listbox listboxData;
	private List<Users> users;
	private Users selectedUser;
	private Logger logger = Logger.getLogger(this.getClass());

	@Override
	protected void initComponent() {
		params = new HashMap<Object, Object>();
		paging.setPageSize(5);
		loadDataUsers(0);
	}

	private void generateDataUsers(final int cursor) {
		int no = cursor + 1;
		params.put("limit", LIMIT);
		params.put("cursor", cursor);
		paging.setTotalSize(usersService.countAllUsers());
		users = usersService.getAll(params);
		generateLisboxData(no);
	}

	public void loadDataUsers(int cursor) {
		// Show Listbox on the first
		generateDataUsers(cursor);
		paging.addEventListener("onPaging", new EventListener() {

			public void onEvent(Event event) throws Exception {
				PagingEvent pagingEvent = (PagingEvent) event;
				int activePage = pagingEvent.getActivePage();
				int cursor = activePage * LIMIT;
				// Redraw current paging
				generateDataUsers(cursor);
			}
		});
	}

	public void generateLisboxData(int no) {
		listboxData.getItems().clear();
		for (final Users user : users) {
			Listitem listitem = new Listitem();
			listitem.appendChild(new Listcell(no + ""));
			listitem.appendChild(new Listcell(user.getUsername()));
			listitem.appendChild(new Listcell(user.getRole().getName()));
			if (user.isActive()) {
				listitem.appendChild(new Listcell("Active"));
			}
			listitem.addEventListener("onClick", new EventListener() {

				public void onEvent(Event event) throws Exception {
					selectedUser = users.get(listboxData.getSelectedIndex());
				}
			});

			Toolbarbutton toolbarbutton = new Toolbarbutton();
			toolbarbutton.setLabel(Labels.getLabel("toolbarBtnPinjaman"));
			toolbarbutton.setImage("img/");
			toolbarbutton.addEventListener("onClick", new EventListener() {

				public void onEvent(Event event) throws Exception {
					// delete(bentukUsaha.getId());
				}
			});
			Listcell listcellPinjaman = new Listcell();
			listcellPinjaman.appendChild(toolbarbutton);
			listitem.appendChild(listcellPinjaman);
			no++;
			listboxData.appendChild(listitem);
		}
	}

}
