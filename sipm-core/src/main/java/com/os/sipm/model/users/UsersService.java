/**
 * 
 */
package com.os.sipm.model.users;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kris Dec 18, 2011 7:37:53 PM
 */
@Service
public class UsersService {

    @Autowired
    private UsersDao usersDao;

    public int saveOrUpdate(Users entity) {
        if (entity.getId() == null) {
            return usersDao.saveUsers(entity);
        } else {
            return usersDao.updateUsers(entity);
        }
    }

    public int delete(Users entity) {
        return usersDao.deleteUsers(entity);
    }

    public Users getById(Integer id) {
        return usersDao.getUserById(id);
    }

    public List<Users> getByName(Map<Object, Object> params) {
        return usersDao.getUserByUsername(params);
    }

    public List<Users> getAll(Map<Object, Object> params) {
        return usersDao.getAllUsers(params);
    }

    public Users getUsersByUsernameAndPassword(Map<Object, Object> params) {
        return usersDao.getUsersByUsernameAndPassword(params);
    }

    public int countAllUsers() {
        return usersDao.countAllUsers();
    }
}
