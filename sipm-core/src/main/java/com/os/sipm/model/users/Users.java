/**
 * 
 */
package com.os.sipm.model.users;

import java.io.Serializable;

import com.os.sipm.model.users.role.Role;

/**
 * @author kris Dec 18, 2011 7:14:35 PM
 */
public class Users implements Serializable {

    private static final long serialVersionUID = 8456933075971700818L;
    private Integer id;
    private String realname;
    private String username;
    private String password;
    private boolean active;
    private Role role;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
