/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.controller.matakuliah.mahasiswa;

import com.os.sipm.controller.OSController;
import com.os.sipm.model.mahasiswa.Mahasiswa;
import com.os.sipm.model.mahasiswa.MahasiswaService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Toolbarbutton;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author kris
 */
public class AddController extends OSController {

    private Listbox listboxData;
    private List<Mahasiswa> mahasiswas;
    private Textbox txtboxNim;
    private Textbox txtboxNama;
    @Autowired
    private MahasiswaService mahasiswaService;
    private Paging paging;
    private final int LIMIT = 2;
    private Map<Object, Object> params;
    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    protected void initComponent() {
        params = new HashMap<Object, Object>();
        loadDataMahasiswa(0);
    }

    public void onClick$btnCariNim() throws InterruptedException {
        if (!txtboxNim.getValue().isEmpty()) {
            params.put("nim", txtboxNim.getValue() + "%");
        }
        if (!txtboxNama.getValue().isEmpty()) {
            params.put("nama", txtboxNama.getValue() + "%");
        }
        loadDataMahasiswa(0);
    }

    public void loadDataMahasiswa(int cursor) {
        // Show Listbox on the first
        generateDataMahasiswa(cursor);
        paging.addEventListener("onPaging", new EventListener() {

            public void onEvent(Event event) throws Exception {
                PagingEvent pagingEvent = (PagingEvent) event;
                int activePage = pagingEvent.getActivePage();
                int cursor = activePage * LIMIT;
                // Redraw current paging
                generateDataMahasiswa(cursor);
            }
        });
    }

    private void generateDataMahasiswa(final int cursor) {
        int no = cursor + 1;
        params.put("limit", LIMIT);
        params.put("cursor", cursor);
        if (params.containsKey("nim")) {
            paging.setTotalSize(mahasiswaService.countAllMahasiswaByNim(params));
            mahasiswas = mahasiswaService.getByNim(params);
        } else if (params.containsKey("nama")) {
            paging.setTotalSize(mahasiswaService.countAllMahasiswaByNama(params));
            mahasiswas = mahasiswaService.getByName(params);
        } else {
            paging.setTotalSize(mahasiswaService.countAllMahasiswa());
            mahasiswas = mahasiswaService.getAll(params);
        }
        generateLisboxData(no);
    }

    public void generateLisboxData(int no) {
        listboxData.getItems().clear();
        for (final Mahasiswa mahasiswa : mahasiswas) {
            Listitem listitem = new Listitem();
            listitem.appendChild(new Listcell(""));
            listitem.appendChild(new Listcell(no + ""));
            listitem.appendChild(new Listcell(mahasiswa.getNim()));
            listitem.appendChild(new Listcell(mahasiswa.getNama()));
            listitem.appendChild(new Listcell(mahasiswa.getJurusan().getNama()));
            listitem.addEventListener("onClick", new EventListener() {

                public void onEvent(Event event) throws Exception {
                    mahasiswas.get(listboxData.getSelectedIndex());
                }
            });
            listboxData.appendChild(listitem);
            no++;
        }
    }
}
