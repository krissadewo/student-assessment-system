/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.controller.matakuliah;

import com.os.sipm.controller.OSController;
import java.util.HashMap;
import java.util.Map;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Textbox;

/**
 * 
 * @author KrisSadewo
 */
public class SearchController extends OSController {
   
	private static final long serialVersionUID = 3424552455140875080L;
	
	private Textbox txtboxKodeCari;
    private Textbox txtboxNamaCari;
    private Checkbox chkboxKode;
    private Checkbox chkboxNama;

    @Override
    protected void initComponent() {
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void onClick$btnCari(Event event) {
        Map params = new HashMap();
        if (!txtboxKodeCari.getValue().isEmpty()) {
            params.put("kode", txtboxKodeCari.getValue() + "%");
        } else if (!txtboxNamaCari.getValue().isEmpty()) {
            params.put("nama", txtboxNamaCari.getValue() + "%");
        }
        self.setAttribute("params", params);
        self.detach();
    }

    public void onClick$chkboxKode(Event event) {
        if (chkboxKode.isChecked()) {
            txtboxKodeCari.setVisible(true);
        } else {
            txtboxKodeCari.setVisible(false);
        }
    }

    public void onClick$chkboxNama(Event event) {
        if (chkboxNama.isChecked()) {
            txtboxNamaCari.setVisible(true);
        } else {
            txtboxNamaCari.setVisible(false);
        }
    }

    public void onClick$btnBatal(Event event) {
        txtboxNamaCari.setValue("");
        txtboxKodeCari.setValue("");
    }
}
