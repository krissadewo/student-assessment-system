/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.controller;

import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Paging;

import com.os.sipm.model.dosen.DosenService;
import com.os.sipm.model.jenisnilai.JenisNilaiService;
import com.os.sipm.model.mahasiswa.MahasiswaService;
import com.os.sipm.model.matakuliah.MataKuliahService;
import com.os.sipm.model.matakuliah.dosen.MataKuliahDosenService;
import com.os.sipm.model.menu.MenuItemService;
import com.os.sipm.model.menu.MenuService;
import com.os.sipm.model.users.UsersService;

/**
 * 
 * @author kris
 * @created on Jul 6, 2011 4:53:58 PM
 */
public abstract class OSController extends GenericForwardComposer {

	private static final long serialVersionUID = 8756454119146265889L;
        
        /**
         * @Autowired has not used again, because we can use zk spring resolver in zul
         */
	protected MahasiswaService mahasiswaService;
	protected DosenService dosenService;
	protected MenuItemService menuItemService;
	protected MenuService menuService;
	protected MataKuliahService mataKuliahService;
	protected JenisNilaiService jenisNilaiService;	
	protected MataKuliahDosenService mataKuliahDosenService;	
	protected UsersService usersService;

	protected final int LIMIT = 5;
	protected Paging paging;
	protected Map<Object, Object> params;


	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		initComponent();
	}

	protected abstract void initComponent();
}
