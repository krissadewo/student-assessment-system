/**
 * 
 */
package com.os.sipm.controller.general;

import java.util.List;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Panelchildren;
import org.zkoss.zul.West;

import com.os.sipm.controller.OSController;
import com.os.sipm.model.menu.Menu;
import com.os.sipm.model.menu.MenuItem;
import com.os.sipm.model.menu.MenuService;
import com.os.sipm.model.users.Users;
import com.os.sipm.model.users.UsersService;

/**
 * @author KrisSadewo Apr 22, 2011
 */
public class IndexController extends OSController {

	private static final long serialVersionUID = 6405381107645509239L;

	private Div content;
	private Div divMainMenu;
	private Div divMenuItem;
	private List<Menu> menus;
	private List<MenuItem> menuItems;
	private West westContent;
	private Logger logger = Logger.getLogger(this.getClass());

	public IndexController() {
		initializeSpecialBean();	
		doLogin();
		createMenu();		
	}

	@Override
	protected void initComponent() {		
		createMenuItem();		
	}
	private void doLogin() {
		logger.info("Creating session");
		Users users = (Users) session.getAttribute("users");
		if (users == null) {
			Executions.getCurrent().sendRedirect("login.zul");			
		}
	}

	private void createMenu() {		
		this.menus = menuService.getAll();
	}

	private void createMenuItem() {
		this.menuItems = menuItemService.getAll();
		boolean visible = true;
		int sequence = 0;
		for (final Menu menu : menus) {
			final Panel panel = new Panel();
			Panelchildren panelchildren = new Panelchildren();
			Listbox listbox = new Listbox();
			for (final MenuItem menuItem : menuItems) {
				if (menu.getMenuName().equalsIgnoreCase(
						menuItem.getMenu().getMenuName())) {
					Listitem listitem = new Listitem();
					Listcell listcell = new Listcell();
					listcell.setImage(menuItem.getImagePath());
					listcell.setLabel(Labels.getLabel(menuItem
							.getMenuItemName()));
					listitem.appendChild(listcell);
					listitem.addEventListener("onClick", new EventListener() {

						public void onEvent(Event arg0) throws Exception {
							content.getChildren().clear();
							Executions.createComponents(menuItem.getViewPath(),
									content, null);
						}
					});
					listbox.setOddRowSclass("non-odd");
					listbox.setStyle("border:none;");
					listbox.appendChild(listitem);
				}
			}
			panelchildren.appendChild(listbox);
			panel.appendChild(panelchildren);
			// Set visible for first panel
			panel.setVisible(visible);
			divMenuItem.appendChild(panel);
			// Set label for each panel, because the properties language
			// can't set the label on event doBeforeComposeChildren
			Button button = (Button) divMainMenu.getChildren().get(sequence);
			button.setLabel(Labels.getLabel(menu.getMenuName()));
			// Set class for first panel
			if (visible) {
				westContent.setTitle(Labels.getLabel(menu.getMenuName()));
				button.setSclass("os-seld");
			}
			// Adding event for each panel
			button.addEventListener("onClick", new EventListener() {

				public void onEvent(Event event) throws Exception {
					divMenuItem.getChildren().clear();
					divMenuItem.appendChild(panel);
					westContent.setTitle(Labels.getLabel(menu.getMenuName()));
					panel.setVisible(true);
				}
			});
			visible = false;
			sequence++;
		}
	}

	private void initializeSpecialBean(){
		session = Sessions.getCurrent(true);
		XmlWebApplicationContext context = new XmlWebApplicationContext();
		context.setServletContext((ServletContext) session.getWebApp().getNativeContext());
		context.refresh();
		menuService = context.getBean(MenuService.class);		
	}
	
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public List<Menu> getMenus() {
		return menus;
	}
}
