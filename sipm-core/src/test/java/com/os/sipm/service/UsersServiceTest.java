/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.service;

import com.os.sipm.model.users.Users;
import com.os.sipm.model.users.UsersService;
import com.os.sipm.model.users.role.Role;

import java.util.HashMap;
import java.util.Map;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author KrisSadewo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:com/os/sipm/config/SpringConfig.xml"})
public class UsersServiceTest {

    @Autowired
    private UsersService userService;

    //@Test
    public void checkInstance() {
        Assert.assertNotNull(userService);
    }

    //@Test
    public void save() {
        Users users = new Users();
        users.setRealname("Kris");
        users.setUsername("kris");
        users.setPassword("kris");
        users.setActive(true);
        
        Role role = new Role();
        role.setId(1);
        
        users.setRole(role);        
        Assert.assertNotNull(userService.saveOrUpdate(users));
    }

    //@Test
    public void get() {
        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("limit", 10);
        params.put("cursor", 0);       
        Assert.assertNotNull(userService.getAll(params));
      
    }
    
    @Test
    public void getUsersByUsernameAndPassword(){
    	 Map<Object, Object> params = new HashMap<Object, Object>();
         params.put("username", "kris");
         params.put("password", "kris");
         Assert.assertNotNull(userService.getUsersByUsernameAndPassword(params));
    }
}
