/**
 * 
 */
package com.os.sipm.model.users.role;

import java.io.Serializable;

/**
 * @author kris Dec 18, 2011 7:15:56 PM
 */
public class Role implements Serializable {

    private static final long serialVersionUID = 390446681240294243L;
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
