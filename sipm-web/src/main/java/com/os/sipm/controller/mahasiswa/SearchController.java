/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.controller.mahasiswa;

import com.os.sipm.controller.OSController;
import java.util.HashMap;
import java.util.Map;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Textbox;

/**
 * 
 * @author KrisSadewo
 */
public class SearchController extends OSController {

	private static final long serialVersionUID = 2622647624171654559L;
	
	private Textbox txtboxNimCari;
    private Textbox txtboxNamaCari;
    private Checkbox chkboxNim;
    private Checkbox chkboxNama;

    @Override
    protected void initComponent() {
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public void onClick$btnCari(Event event) {
        Map params = new HashMap();
        if (!txtboxNimCari.getValue().isEmpty()) {
            params.put("nim", txtboxNimCari.getValue() + "%");
        } else if (!txtboxNamaCari.getValue().isEmpty()) {
            params.put("nama", txtboxNamaCari.getValue() + "%");
        }
        self.setAttribute("params", params);
        self.detach();
    }

    public void onClick$chkboxNim(Event event) {
        if (chkboxNim.isChecked()) {
            txtboxNimCari.setVisible(true);
        } else {
            txtboxNimCari.setVisible(false);
        }
    }

    public void onClick$chkboxNama(Event event) {
        if (chkboxNama.isChecked()) {
            txtboxNamaCari.setVisible(true);
        } else {
            txtboxNamaCari.setVisible(false);
        }
    }

    public void onClick$btnBatal(Event event) {
        txtboxNamaCari.setValue("");
        txtboxNimCari.setValue("");
    }
}
