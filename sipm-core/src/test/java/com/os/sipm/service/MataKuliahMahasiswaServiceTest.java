/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.service;

import com.os.sipm.model.mahasiswa.Mahasiswa;
import com.os.sipm.model.matakuliah.MataKuliah;
import com.os.sipm.model.matakuliah.mahasiswa.MataKuliahMahasiswa;
import com.os.sipm.model.matakuliah.mahasiswa.MataKuliahMahasiswaService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author KrisSadewo
 * Jul 5, 2011   
 * 11:40:28 AM 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:com/os/sipm/config/SpringConfig.xml"})
public class MataKuliahMahasiswaServiceTest {

    @Autowired
    private MataKuliahMahasiswaService mataKuliahMahasiswaService;

    //@Test
    public void insert() {
        List<MataKuliahMahasiswa> mataKuliahMahasiswas = new ArrayList<MataKuliahMahasiswa>();
        MataKuliahMahasiswa mataKuliahMahasiswa = new MataKuliahMahasiswa();
        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(1);
        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setId(2);
        mataKuliahMahasiswa.setMataKuliah(mataKuliah);
        mataKuliahMahasiswa.setMahasiswa(mahasiswa);
        mataKuliahMahasiswas.add(mataKuliahMahasiswa);
        mataKuliahMahasiswas.add(mataKuliahMahasiswa);
        mataKuliahMahasiswas.add(mataKuliahMahasiswa);
        mataKuliahMahasiswas.add(mataKuliahMahasiswa);
        mataKuliahMahasiswas.add(mataKuliahMahasiswa);
        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("params", mataKuliahMahasiswas);
        params.put("save", "save");
        Assert.assertEquals(mataKuliahMahasiswaService.saveOrUpdate(params), 5);
    }

    //@Test
    public void update() {

        List<MataKuliahMahasiswa> mataKuliahMahasiswas = new ArrayList<MataKuliahMahasiswa>();
        MataKuliahMahasiswa mataKuliahMahasiswa1 = new MataKuliahMahasiswa();
        mataKuliahMahasiswa1.setId(1);
        MataKuliahMahasiswa mataKuliahMahasiswa2 = new MataKuliahMahasiswa();
        mataKuliahMahasiswa2.setId(2);
        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(10);
        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setId(12);
        mataKuliahMahasiswa1.setMataKuliah(mataKuliah);
        mataKuliahMahasiswa1.setMahasiswa(mahasiswa);
        mataKuliahMahasiswa2.setMataKuliah(mataKuliah);
        mataKuliahMahasiswa2.setMahasiswa(mahasiswa);

        mataKuliahMahasiswas.add(mataKuliahMahasiswa1);
        mataKuliahMahasiswas.add(mataKuliahMahasiswa2);

        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("params", mataKuliahMahasiswas);
        Assert.assertEquals(mataKuliahMahasiswaService.saveOrUpdate(params), 2);
    }

    @Test
    public void delete() {
        List<MataKuliahMahasiswa> mataKuliahMahasiswas = new ArrayList<MataKuliahMahasiswa>();
        MataKuliahMahasiswa mataKuliahMahasiswa1 = new MataKuliahMahasiswa();
        mataKuliahMahasiswa1.setId(1);
        MataKuliahMahasiswa mataKuliahMahasiswa2 = new MataKuliahMahasiswa();
        mataKuliahMahasiswa2.setId(2);
        mataKuliahMahasiswas.add(mataKuliahMahasiswa1);
        mataKuliahMahasiswas.add(mataKuliahMahasiswa2);

    }
}
