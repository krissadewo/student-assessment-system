/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.model.mahasiswa.nilai;

import com.os.sipm.model.matakuliah.mahasiswa.MataKuliahMahasiswa;

/**
 *
 * @author KrisSadewo
 */
public class NilaiMahasiswa {

    private Integer id;
    private float nilai;
    private MataKuliahMahasiswa mataKuliahMahasiswa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MataKuliahMahasiswa getMataKuliahMahasiswa() {
        return mataKuliahMahasiswa;
    }

    public void setMataKuliahMahasiswa(MataKuliahMahasiswa mataKuliahMahasiswa) {
        this.mataKuliahMahasiswa = mataKuliahMahasiswa;
    }

    public float getNilai() {
        return nilai;
    }

    public void setNilai(float nilai) {
        this.nilai = nilai;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NilaiMahasiswa other = (NilaiMahasiswa) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (Float.floatToIntBits(this.nilai) != Float.floatToIntBits(other.nilai)) {
            return false;
        }
        if (this.mataKuliahMahasiswa != other.mataKuliahMahasiswa && (this.mataKuliahMahasiswa == null || !this.mataKuliahMahasiswa.equals(other.mataKuliahMahasiswa))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 71 * hash + Float.floatToIntBits(this.nilai);
        hash = 71 * hash + (this.mataKuliahMahasiswa != null ? this.mataKuliahMahasiswa.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "NilaiMahasiswa{" + "id=" + id + ", nilai=" + nilai + ", mataKuliahMahasiswa=" + mataKuliahMahasiswa + '}';
    }
}
