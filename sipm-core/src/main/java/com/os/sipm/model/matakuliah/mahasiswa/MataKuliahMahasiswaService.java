/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.model.matakuliah.mahasiswa;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author KrisSadewo
 * Jul 5, 2011   
 * 11:38:52 AM 
 */
@Service
public class MataKuliahMahasiswaService {

    @Autowired
    private MataKuliahMahasiswaDao matakuliahMahasiswaDao;

    public int saveOrUpdate(Map<Object, Object> mataKuliahMahasiswas) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int delete(MataKuliahMahasiswa mataKuliahMahasiswa) {
        return matakuliahMahasiswaDao.deleteMataKuliahMahasiswa(mataKuliahMahasiswa);
    }

    public MataKuliahMahasiswa getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<MataKuliahMahasiswa> getByName(Map<Object, Object> params) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<MataKuliahMahasiswa> getAll(Map<Object, Object> params) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
