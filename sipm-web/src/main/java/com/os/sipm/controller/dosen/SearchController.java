/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.controller.dosen;

import com.os.sipm.controller.OSController;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Textbox;

/**
 * 
 * @author KrisSadewo
 */
public class SearchController extends OSController {

    private static final long serialVersionUID = -2302671582819992376L;
	
    private Textbox txtboxNipCari;
    private Textbox txtboxNamaCari;
    private Checkbox chkboxNip;
    private Checkbox chkboxNama;

    @Override
    protected void initComponent() {
        //Not implemented yet ...
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public void onClick$btnCari(Event event) {
        Map params = new HashMap();
        if (!txtboxNipCari.getValue().isEmpty()) {
            params.put("nip", txtboxNipCari.getValue() + "%");
        } else if (!txtboxNamaCari.getValue().isEmpty()) {
            params.put("nama", txtboxNamaCari.getValue() + "%");
        }
        self.setAttribute("params", params);
        self.detach();
    }

    public void onClick$chkboxNip(Event event) {
        if (chkboxNip.isChecked()) {
            txtboxNipCari.setVisible(true);
        } else {
            txtboxNipCari.setVisible(false);
        }
    }

    public void onClick$chkboxNama(Event event) {
        if (chkboxNama.isChecked()) {
            txtboxNamaCari.setVisible(true);
        } else {
            txtboxNamaCari.setVisible(false);
        }
    }

    public void onClick$btnBatal(Event event) {
        txtboxNamaCari.setValue("");
        txtboxNipCari.setValue("");
    }
}
