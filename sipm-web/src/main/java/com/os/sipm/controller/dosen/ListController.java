/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.sipm.controller.dosen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

import com.os.sipm.controller.OSController;
import com.os.sipm.helper.MessagesHelper;
import com.os.sipm.model.dosen.Dosen;

/**
 * 
 * @author KrisSadewo
 */
public class ListController extends OSController {

	private static final long serialVersionUID = 1238501949462736599L;

	private Listbox listboxData;
	private List<Dosen> dosens;
	private Dosen selectedDosen;
	private Logger logger = Logger.getLogger(this.getClass());

	@Override
	protected void initComponent() {
		params = new HashMap<Object, Object>();
		paging.setPageSize(5);
		loadDataDosen(0);
	}

	private void generateDataDosen(final int cursor) {
		int no = cursor + 1;
		params.put("limit", LIMIT);
		params.put("cursor", cursor);
		if (params.containsKey("nip")) {
			paging.setTotalSize(dosenService.countAllDosenByNip(params));
			dosens = dosenService.getByNip(params);
		} else if (params.containsKey("nama")) {
			paging.setTotalSize(dosenService.countAllDosenByName(params));
			dosens = dosenService.getByName(params);
		} else {
			paging.setTotalSize(dosenService.countAllDosen());
			dosens = dosenService.getAll(params);
			logger.info(dosens.size());
		}
		generateLisboxData(no);
	}

	public void loadDataDosen(int cursor) {
		// Show Listbox on the first
		generateDataDosen(cursor);
		paging.addEventListener("onPaging", new EventListener() {

			public void onEvent(Event event) throws Exception {
				PagingEvent pagingEvent = (PagingEvent) event;
				int activePage = pagingEvent.getActivePage();
				int cursor = activePage * LIMIT;
				// Redraw current paging
				generateDataDosen(cursor);
			}
		});
	}

	public Listbox generateLisboxData(int no) {
		listboxData.getItems().clear();
		for (final Dosen dosen : dosens) {
			Listitem listitem = new Listitem();
			listitem.appendChild(new Listcell(no + ""));
			listitem.appendChild(new Listcell(dosen.getNip()));
			listitem.appendChild(new Listcell(dosen.getNama()));
			listitem.addEventListener("onClick", new EventListener() {

				public void onEvent(Event event) throws Exception {
					selectedDosen = dosens.get(listboxData.getSelectedIndex());
				}
			});
			no++;
			listboxData.appendChild(listitem);
		}
		return listboxData;
	}

	@SuppressWarnings("unchecked")
	public void onClick$btnAdd(Event event) throws InterruptedException {
		Window window = (Window) Executions.createComponents("/views/dosen/add.zul", this.self, null);
		window.doModal();
		List<Dosen> dosenTemp = (List<Dosen>) window.getAttribute("dosens");
		if (dosenTemp != null) {
			dosens = dosenTemp;
			this.generateLisboxData(1);
		}
	}

	@SuppressWarnings("unchecked")
	public void onClick$btnEdit(Event event) throws InterruptedException {
		if (listboxData.getSelectedIndex() < 0) {
			MessagesHelper.editEmpty();
		} else {
			// Send param to modal window
			param.put("selectedDosen", selectedDosen);
			Window window = (Window) Executions.createComponents("/views/dosen/add.zul", this.self, param);
			window.doModal();
			List<Dosen> dosenTemp = (List<Dosen>) window.getAttribute("dosens");
			if (dosenTemp != null) {
				dosens = dosenTemp;
				this.generateLisboxData(1);
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void onClick$btnSearch(Event event) throws InterruptedException {
		Window window = (Window) Executions.createComponents("/views/dosen/search.zul", this.self, null);
		window.doModal();
		params = (Map) window.getAttribute("params");
		if (params != null) {
			paging.setTotalSize(0);
			loadDataDosen(0);
		}
	}

	public void onClick$btnDelete(final Event event) throws InterruptedException {
		if (listboxData.getSelectedIndex() < 0) {
			MessagesHelper.deleteEmpty();
		} else {
			Messagebox.show(MessagesHelper.comfirmationDelete(), "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener() {

						public void onEvent(Event evt) {
							switch (((Integer) evt.getData()).intValue()) {
							case Messagebox.YES:
								if (dosenService.delete(selectedDosen) == 1) {
									MessagesHelper.deleteSuccess();
									// Remove data from listbox with unique id
									// from
									// selectedIndex
									dosens.remove(listboxData.getSelectedIndex());
									onClick$btnRefresh(event);
								} else {
									MessagesHelper.deleteFailed();
								}
								break;
							case Messagebox.NO:
								break;
							}
						}
					});
		}
	}

	public void onClick$btnRefresh(Event event) {
		loadDataDosen(paging.getActivePage() * LIMIT);
	}
}
