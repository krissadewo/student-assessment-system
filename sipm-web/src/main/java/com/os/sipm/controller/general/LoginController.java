package com.os.sipm.controller.general;

import java.util.HashMap;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Textbox;

import com.os.sipm.controller.OSController;
import com.os.sipm.helper.MessagesHelper;
import com.os.sipm.model.users.Users;

public class LoginController extends OSController {

	private static final long serialVersionUID = -5796057167680064146L;
	private Textbox txtboxUsername;
	private Textbox txtboxPassword;

	@Override
	protected void initComponent() {
		params = new HashMap<Object, Object>();
	}

	public void onClick$btnLogin(Event event) {
		params.put("username", txtboxUsername.getText());
		params.put("password", txtboxPassword.getText());
		Users users = usersService.getUsersByUsernameAndPassword(params);
		if (users != null) {
			if (users.isActive()) {
				session.setAttribute("users", users);
				this.execution.sendRedirect("index.zul");
			} else {
				MessagesHelper.usersNotActived();
			}
		} else {
			MessagesHelper.loginFailed();
		}
	}

}
